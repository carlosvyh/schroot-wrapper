#!/bin/bash

SCHROOT_NAME=""
EXEC_ENVIRONMENT="local"

execute_do_base()
{
    ##Si se invoca directamente al comando do_chroot (Si se quiere invocar algun comando no comtemplado en la lista de comandos)
    if [ "$SCRIPT_NAME" == "$COMMAND_NAME" ]
    then
        ## Si se ejecuta dentro de un SCHROOT
        if [[ "$SCHROOT_NAME" != "" ]]
        then
            COMMAND_NAME="$COMMAND_PARAMETERS"
            COMMAND_PARAMETERS=""
            exec_in_schroot
        else
            echo "ERROR: no se a especificado el schroot"
        fi
    else
        ## Si se ejecuta dentro de un SCHROOT
        if [[ "$SCHROOT_NAME" != "" ]]
        then
            exec_in_schroot
        else
            exec_in_local $COMMAND_PARAMETERS
        fi
    fi
}

exec_in_schroot_base()
{
    local _schroot_name=$SCHROOT_NAME
    local _schroot_command="source ~/env.sh ; $COMMAND_NAME $COMMAND_PARAMETERS"

    if [[ "${SCHROOT_CURRENT_DIR}" = "" ]]
    then
        SCHROOT_CURRENT_DIR=$PWD
    fi

    local _schroot_session_name=${_schroot_name}_${USER}

    _create_session $_schroot_name

    #echo "schroot -r -c $_schroot_session_name --directory=$SCHROOT_CURRENT_DIR -- bash -c \"export PROJECT_BASE_DIR=$PROJECT_BASE_DIR; export CI_COMMIT_BRANCH=$CI_COMMIT_BRANCH; $_schroot_command\""
    schroot -r -c $_schroot_session_name --directory=$SCHROOT_CURRENT_DIR -- bash -c "export PROJECT_BASE_DIR=$PROJECT_BASE_DIR; export CI_COMMIT_BRANCH=$CI_COMMIT_BRANCH; $_schroot_command"
}

exec_in_local_base()
{
    _commandName=`basename $0`
    _commandPath=$(choose_local_command $_commandName)
    $_commandPath $@
}

choose_local_command_base()
{
    _commandPath='/usr/bin/'$1
    case "$1" in
       *)
          ;;
    esac
    echo $_commandPath
}

choose_environment_base()
{
    local _foundChroot=false
    local _foundCommand=false

    local _buildMode=$1
    local _commandRequired=$2

    for _chrootKey in "${!ACCEPTED_SCHROOTS[@]}"
    do 
        if [[ "$_chrootKey" == "$_buildMode" ]]
        then
            _foundChroot=true
            SCHROOT_NAME=${ACCEPTED_SCHROOTS[$_buildMode]}
            EXEC_ENVIRONMENT="schroot"
            #echo "schroot encontrado -> $_buildMode"
            break
        fi
    done

    if [[ "$_foundChroot" == "true" ]]; then

        for _command in "${ACCEPTED_COMMANDS[@]}"
        do
            if [[ "$_command" == "$_commandRequired" ]]
            then
                _foundCommand=true
                #echo "Comando encontrado -> $_commandRequired"
            break
            fi
        done

        if [[ "$_foundCommand" == "false" ]]
        then
            echo "Error: Wrapper no soporta este comando -> $_commandRequired"
            exit 1
        fi
        #else
        #echo "Error: Schroot no encontrado -> $_buildMode"
        #exit 1
    fi
}

_create_session()
{
    _schroot_name=$1
    _schroot_session_name=${_schroot_name}_${USER}
    _session_file="/tmp/${_schroot_session_name}_file"


    foundSession=false
    foundFile=false

    _sessions=$(schroot --list --all-sessions)

    for SESSION in $_sessions
    do 
        if [ "session:$_schroot_session_name" == "$SESSION" ]
        then
            foundSession=true
            break
        fi
    done

    if [ -e $_session_file ]
    then
        foundFile=true
    fi

    if [[ "$foundSession" == "false"   || "$foundFile" == "false" ]] 
    then
        #echo "Eliminando session de ${_schroot_session_name}"
        schroot -e --chroot ${_schroot_session_name} 2>/dev/null
        #echo "Creando la session de ${_schroot_session_name}"
        schroot -b -c${_schroot_name} --session-name ${_schroot_session_name} 2>/dev/null
        echo $_schroot_session_name > $_session_file
        #echo "Nueva session de ${_schroot_session_name} : "$_schroot_session_name
    fi
}

binpath="$(dirname "$0")"

if [ ! -f "$binpath/library_custom.sh" ]; then
cat <<'EOL'  > $binpath/library_custom.sh
#!/bin/bash

ACCEPTED_COMMANDS=( c++ cmake do_chroot g++ gcc gdb make qmake xfce4-terminal )
declare -A ACCEPTED_SCHROOTS=( [deb832-v1]=debian8-32-v1 [deb832-v2]=debian8-32-v2 [deb964-v1]=debian9-64-v1 )

commandLocalPathQmake="/path/to/qmake"
commandLocalPathMake="/path/to/make"

exec_in_schroot()
{
    exec_in_schroot_base "$@"
}

exec_in_local()
{
    exec_in_local_base $@
}

choose_local_command()
{
    choose_local_command_base $@
}

choose_environment()
{
    choose_environment_base $1 $2
}

execute_do()
{
    execute_do_base
}

to_schroot()
{
    to_schroot_base
}

EOL
fi

source $binpath/library_custom.sh
