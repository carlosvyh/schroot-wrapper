**GENERALITIES**

This wrapper enable to execute severals command into schroots directly from your current hosts prompt hidding the complex elaborate command lines of schroot command. It enable executation of command in diferents schroots, to do that is necesary to export a global environment variable that define the desired schroot.
<br><br>

*BUILD\_MODE*

Environment variable that define the desired schroot that want to work with. It have to be into array ACCEPTED\_SCHROOTS described in library\_custom, that will be created
after first executation.
<br><br>

*library\_base.sh file*

Define base functions.<br>
Create library\_custom.sh file if dons't exists the first time when you execute some command in $HOME/bin (read bellow)
<br><br>

*library\_custom.sh file*

It call base functions in library\_base
Here you can customize each base function
Here Define the commands and schroot's acepted in:

```
ACCEPTED_COMMANDS=( c++ cmake do_chroot g++ gcc gdb make qmake xfce4-terminal )
declare -A ACCEPTED_SCHROOTS=( [deb832-v1]=debian8-32-v1 [deb832-v2]=debian8-32-v2 [deb964-v1]=debian9-64-v1 )
```

**FUNCTIONS**

*to\_schroot* function

It use the wrapper to go into the desired schroot and open xfce4-terminal
<br><br>

*do\_schroot* file

wrapper to execute commands (accepted) into the desired schroot.

**Host configuration**

Create $HOME/bin directory.
For each command (permited) that you what to execute into schroot, you have to create a soft link into $HOME/bin to $HOME/schroot-wrapper-/do\_schroot. These command have to be into array ACCEPTED\_COMMANDS described in library\_custom.sh

COMMAND\_NAME
COMMAND\_NAME\_PARAMETERS
SCHROOT\_NAME
EXEC\_ENVIRONMENT

Schroot configurations
---

I prefer dont contamine schroot with host enviroment variables.
My schroot user is the same of host user
In $HOME into schroot clone XXXXXXXXXXXXXXXXX
In $HOME into schroot create a bin directory and enable this into $PATH
//<==================For each command that you want to execute into schroot from host, you have to create a softlink init $HOME/bin